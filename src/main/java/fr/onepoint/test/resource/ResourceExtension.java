/*
 * Copyright 2018 Onepoint
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package fr.onepoint.test.resource;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.io.File;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.text.MessageFormat.format;

/**
 * Initializes Fields and Parameters annotated with {@link ResourceFile}.
 * All Fields and Parameters of supported type will be initialized.
 * If the resource path specified as the {@link ResourceFile} value can't be reached, an exception will be thrown.
 * Unsupported types will be initialized to null.
 * Parameters are read by the junit5 {@link ParameterResolver}
 * Supported type are the following:
 * <ul>
 *     <li>{@link File}</li>
 *     <li>{@link Path}</li>
 * </ul>
 */
public class ResourceExtension implements ParameterResolver, BeforeEachCallback {

    private Map<Class, Function<File, ?>> converters;

    //To support a new class, add a new entry into the map.
    //The fisrt param is the class to support, and the second a lambda
    //converting an instance of java.io.File to an instance of the key class
    ResourceExtension(){
        converters = new HashMap<>();
        converters.put(File.class, Function.identity());
        converters.put(Path.class, File::toPath);
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        for(Field field : getEligibleFields(extensionContext)){
            field.set(extensionContext.getRequiredTestInstance(), getObject(field.getAnnotation(ResourceFile.class).value(), field.getType()));
        }
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return isEligible(parameterContext.getParameter(), parameterContext.getParameter().getType());
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        Parameter param = parameterContext.getParameter();
        String annotationValue = param.getAnnotation(ResourceFile.class).value();
        Class<?> type = param.getType();
        try {
            return getObject(annotationValue, type);
        } catch (URISyntaxException e) {
            throw new ParameterResolutionException(format("Failed to initialize parameter {0}", param.getName()),e);
        }
    }

    private List<Field> getEligibleFields(ExtensionContext extensionContext){
        return Stream.of(extensionContext.getRequiredTestClass().getDeclaredFields())
                .filter(field -> isEligible(field, field.getType()))
                .peek(field -> field.setAccessible(true))
                .collect(Collectors.toList());
    }

    private boolean isEligible(AnnotatedElement element, Class type) {
        return element.isAnnotationPresent(ResourceFile.class) && converters.keySet().contains(type);
    }

    private Object getObject(String annotationValue, Class<?> type) throws URISyntaxException {
        File file = new File(getClass().getClassLoader().getResource(annotationValue).toURI());
        return converters.get(type).apply(file);
    }

}
