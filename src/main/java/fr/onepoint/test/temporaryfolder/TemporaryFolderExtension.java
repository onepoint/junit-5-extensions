/*
 * Copyright 2018 Onepoint
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package fr.onepoint.test.temporaryfolder;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.rules.TemporaryFolder;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Initializes and deletes {@link org.junit.rules.TemporaryFolder} fields in a test class.
 * All {@link org.junit.rules.TemporaryFolder} fields annotated with {@link TmpFolder} will be considered by the extension.
 *
 * If an eligible field is null it will be initialized, else the existing instance will be used by the extension.
 *
 * It will throws IllegalArgumentException when the annotated field is not assignable to TemporaryFolder
 */
public class TemporaryFolderExtension implements BeforeEachCallback, AfterEachCallback {

    @Override
    public void beforeEach(ExtensionContext extensionContext) throws Exception {
        for(TemporaryFolder temporaryFolder : getEligibleFields(extensionContext)){
            temporaryFolder.create();
        }
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) throws Exception {
        for(TemporaryFolder temporaryFolder : getEligibleFields(extensionContext)){
            temporaryFolder.delete();
        }
    }

    private TemporaryFolder getTemporaryFolder(ExtensionContext extensionContext, Field field) throws IllegalAccessException {
        TemporaryFolder temporaryFolder = (TemporaryFolder) field.get(extensionContext.getRequiredTestInstance());
        if(temporaryFolder == null){
            temporaryFolder = new TemporaryFolder();
            field.set(extensionContext.getRequiredTestInstance(), temporaryFolder);
        }
        return temporaryFolder;
    }

    private List<TemporaryFolder> getEligibleFields(ExtensionContext extensionContext) throws IllegalAccessException {
        List<TemporaryFolder> tmpFolders = new ArrayList<>();
        for(Field field : extensionContext.getRequiredTestClass().getDeclaredFields()){
            if(field.isAnnotationPresent(TmpFolder.class)){
                if(!TemporaryFolder.class.isAssignableFrom(field.getType())){
                    throw new ClassCastException(MessageFormat.format("Fail to initialize field {0} : wrong type found. Expected TemporaryFolder or subclass, found {1}", field.getName(), field.getType()));
                }
                field.setAccessible(true);
                tmpFolders.add(getTemporaryFolder(extensionContext, field));
            }
        }
        return tmpFolders;
    }

}
