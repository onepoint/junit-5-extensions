/*
 * Copyright 2018 Onepoint
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package fr.onepoint.test.mockito;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.mockito.MockitoAnnotations;

/**
 * Before each execution , calls {@link org.mockito.MockitoAnnotations#initMocks(Object)} to init all mocks of the current test instance.
 * Following mockito annotations will be initialized :
 * <ul>
 *     <li>{@link org.mockito.Mock}</li>
 *     <li>{@link org.mockito.Spy}</li>
 *     <li>{@link org.mockito.Captor}</li>
 *     <li>{@link org.mockito.InjectMocks}</li>
 * </ul>
 * The test class must be annotated with {@link org.junit.jupiter.api.extension.ExtendWith}({@link MockitoExtension}.class).
 * @deprecated use MockitoExtension from org.mockito::mockito-junit-jupiter instead
 */
@Deprecated
public class MockitoExtension implements BeforeEachCallback {

    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        MockitoAnnotations.initMocks(extensionContext.getRequiredTestInstance());
    }
}
