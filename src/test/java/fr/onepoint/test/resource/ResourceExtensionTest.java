/*
 * Copyright 2018 Onepoint
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package fr.onepoint.test.resource;

import fr.onepoint.test.mockito.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.mockito.Mock;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ResourceExtensionTest {

    private ResourceExtension resourceExtension;

    @Mock
    private ParameterContext parameterContext;

    @Mock
    private ExtensionContext extensionContext;

    private Method method;

    @BeforeEach
    void setup() {
        resourceExtension = new ResourceExtension();
        method = DummyClassTest.class.getMethods()[0];
        when(extensionContext.getRequiredTestClass()).then(call -> DummyClassTest.class);
        when(extensionContext.getRequiredTestInstance()).thenReturn(new DummyClassTest());
    }

    private class DummyClassTest{

        @ResourceFile("dummy.txt") File fileField;
        @ResourceFile("dummy.txt") Path pathField;
        @ResourceFile("dummy.txt") String notSupportedField;
        File notAnnotatedField;

        public void methodWithParameter(
                @ResourceFile("dummy.txt") File fileParam,
                @ResourceFile("dummy.txt") Path pathParam,
                @ResourceFile("dummy.txt") String notSupportedParam,
                File notAnnotatedParam
        ){}
    }

    @Test
    @DisplayName("Initialize File parameters")
    void it_should_initialize_file_annotated_parameter() {
        //Given a test class with a file parameter annotated
        Parameter param = method.getParameters()[0];
        when(parameterContext.getParameter()).thenReturn(param);

        //When ResourceExtension.resolveParameter is called
        Object returnedObject = resourceExtension.resolveParameter(parameterContext, extensionContext);

        //Then the initialized file should be returned
        assertThat(returnedObject).isInstanceOf(File.class);
        File returnedFile = (File) returnedObject;
        assertThat(returnedFile).exists()
                .hasName("dummy.txt")
                .hasContent("the content of the file");

    }

    @Test
    @DisplayName("Initialize Path parameters")
    void it_should_initialize_path_type_annotated_parameter() {
        //Given a test class with a path parameter annotated
        Parameter param = method.getParameters()[1];
        when(parameterContext.getParameter()).thenReturn(param);

        //When ResourceExtension.resolveParameter is called
        Object returnedObject = resourceExtension.resolveParameter(parameterContext, extensionContext);

        //Then the initialized path should be returned
        assertThat(returnedObject).isNotNull().isInstanceOf(Path.class);
        Path returnedPath = (Path) returnedObject;
        assertThat(returnedPath).exists()
                .hasFileName("dummy.txt")
                .hasContent("the content of the file");

    }

    @Test
    @DisplayName("File parameters should be supported")
    void it_should_support_file_parameters() {
        //Given a test class with a not supported parameter annotated
        Parameter param = method.getParameters()[0];
        when(parameterContext.getParameter()).thenReturn(param);

        //When ResourceExtension.supportsParameter is called
        boolean isSupported = resourceExtension.supportsParameter(parameterContext, extensionContext);

        //Then false be returned
        assertThat(isSupported).isTrue();
    }

    @Test
    @DisplayName("Path parameters should be supported")
    void it_should_support_path_parameters() {
        //Given a test class with a not supported parameter annotated
        Parameter param = method.getParameters()[1];
        when(parameterContext.getParameter()).thenReturn(param);

        //When ResourceExtension.supportsParameter is called
        boolean isSupported = resourceExtension.supportsParameter(parameterContext, extensionContext);

        //Then false be returned
        assertThat(isSupported).isTrue();
    }

    @Test
    @DisplayName("Not support parameters with unsupported type")
    void it_should_not_try_to_initialize_parameter_annotated_with_unsupported_type() {
        //Given a test class with a not supported parameter annotated
        Parameter param = method.getParameters()[2];
        when(parameterContext.getParameter()).thenReturn(param);

        //When ResourceExtension.supportsParameter is called
        boolean isSupported = resourceExtension.supportsParameter(parameterContext, extensionContext);

        //Then false be returned
        assertThat(isSupported).isFalse();
    }

    @Test
    @DisplayName("Not initialize not annotated parameters")
    void it_should_not_support_parameter_not_annotated() {
        //Given a test class with a not annotated parameter
        Parameter param = method.getParameters()[3];
        when(parameterContext.getParameter()).thenReturn(param);

        //When ResourceExtension.supportsParameter is called
        boolean isSupported = resourceExtension.supportsParameter(parameterContext, extensionContext);

        //Then false be returned
        assertThat(isSupported).isFalse();
    }

    @Test
    @DisplayName("Initialize File fields")
    void it_should_initialize_file_field_annotated() throws Exception {
        //Given a test class with a file field annotated
        Field field = DummyClassTest.class.getDeclaredField("fileField");

        //When ResourceExtension.supportsParameter is called
        resourceExtension.beforeEach(extensionContext);

        //Then the initialized file should be returned
        File file = (File) field.get(extensionContext.getRequiredTestInstance());
        assertThat(file).isNotNull()
                .hasName("dummy.txt")
                .hasContent("the content of the file");

    }

    @Test
    @DisplayName("Initialize Path fields")
    void it_should_initialize_path_field_annotated() throws Exception {
        //Given a test class with a path filed annotated
        Field field = DummyClassTest.class.getDeclaredField("pathField");

        //When ResourceExtension.supportsParameter is called
        resourceExtension.beforeEach(extensionContext);

        //Then the initialized path should be returned
        Path path = (Path) field.get(extensionContext.getRequiredTestInstance());
        assertThat(path).isNotNull()
                .hasFileName("dummy.txt")
                .hasContent("the content of the file");
    }

    @Test
    @DisplayName("Not support fields with unsupported type")
    void it_should_not_initialize_fields_annotated_with_unsupported_type() throws Exception {
        //Given a test class with an annotated field with an unsupported type
        Field field = DummyClassTest.class.getDeclaredField("notSupportedField");

        //When ResourceExtension.supportsParameter is called
        resourceExtension.beforeEach(extensionContext);

        //Then the field should not be initialized
        Object fieldValue = field.get(extensionContext.getRequiredTestInstance());
        assertThat(fieldValue).isNull();
    }

    @Test
    @DisplayName("Not initialize not annotated fields")
    void it_should_not_support_fields_not_annotated() throws Exception{
        //Given a test class with a parameter not annotated
        Field field = DummyClassTest.class.getDeclaredField("notAnnotatedField");

        //When ResourceExtension.supportsParameter is called
        resourceExtension.beforeEach(extensionContext);

        //Then false be returned
        Object fieldValue = field.get(extensionContext.getRequiredTestInstance());
        assertThat(fieldValue).isNull();
    }

}
