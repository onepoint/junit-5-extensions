/*
 * Copyright 2018 Onepoint
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package fr.onepoint.test.temporaryfolder;

import fr.onepoint.test.mockito.MockitoExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TemporaryFolderExtensionTest {

    private TemporaryFolderExtension temporaryFolderExtension = new TemporaryFolderExtension();

    @Mock
    private ExtensionContext extensionContext;

    private class DummyTestClass{
        @TmpFolder private TemporaryFolder folder = new TemporaryFolder();
    }

    private class DummyTestClassWithoutAnnotation{
        @TmpFolder private TemporaryFolder folderOne = new TemporaryFolder();
        private TemporaryFolder folderTwo = new TemporaryFolder();
    }

    private class DummyTestClassWithOtherClassAnnotated {
        @TmpFolder private TemporaryFolder folder = new TemporaryFolder();
        @TmpFolder private String otherField;
    }

    private class DummyTestWithNullClass{
        @TmpFolder private TemporaryFolder nullFolder;
    }

    @Test
    @DisplayName("Only annotated fields should be considered")
    void it_should_detect_only_annotated_fields() throws Exception{
        //Given an extensionContext returning an instance with a field annoted and another not annotated
        DummyTestClassWithoutAnnotation testClass = new DummyTestClassWithoutAnnotation();
        when(extensionContext.getRequiredTestClass()).thenAnswer(call -> DummyTestClassWithoutAnnotation.class);
        when(extensionContext.getRequiredTestInstance()).thenReturn(testClass);

        //When TemporaryFolderExtension.beforeEach is called
        temporaryFolderExtension.beforeEach(extensionContext);

        //Then only the first filed should be treated
        assertAll(
                () -> assertNotNull(testClass.folderOne),
                () -> assertThrows(IllegalStateException.class, () -> testClass.folderTwo.getRoot())
        );
    }

    @Test
    @DisplayName("Other annotated field type should raise exception")
    void it_should_raise_exception_for_non_TemporaryFolder_fields() {
        //Given an extensionContext returning an instance with one TemporaryFolder annoted but the other one of another instance that TemporaryFolder
        DummyTestClassWithOtherClassAnnotated testClass = new DummyTestClassWithOtherClassAnnotated();
        when(extensionContext.getRequiredTestClass()).thenAnswer(call -> DummyTestClassWithOtherClassAnnotated.class);
        when(extensionContext.getRequiredTestInstance()).thenReturn(testClass);

        //When TemporaryFolderExtension.beforeEach is called
        //Then an exception should be thrown
        assertThatThrownBy(() -> temporaryFolderExtension.beforeEach(extensionContext))
                .isInstanceOf(ClassCastException.class)
                .hasMessage("Fail to initialize field otherField : wrong type found. Expected TemporaryFolder or subclass, found class java.lang.String");

    }

    @Test
    @DisplayName("Annotated TemporaryFolder should be initialized")
    void it_should_create_annotated_TemporaryFolder() throws Exception {
        //Given an extensionContext returning a test having as field a TemporaryFolder annotated with @TmpFolder
        DummyTestClass testClass = new DummyTestClass();
        when(extensionContext.getRequiredTestClass()).thenAnswer(call -> DummyTestClass.class);
        when(extensionContext.getRequiredTestInstance()).thenReturn(testClass);

        //When TemporaryFolderExtension.beforeEach is called
        temporaryFolderExtension.beforeEach(extensionContext);

        //Then the temporaryFolder should be initialized
        assertNotNull(testClass.folder.getRoot());
    }

    @Test
    @DisplayName("Annotated TemporaryFolder should be initialized")
    void it_should_intanciate_null_annotated_TemporaryFolder() throws Exception {
        //Given an extensionContext returning a test having as field a TemporaryFolder annotated with @TmpFolder
        DummyTestWithNullClass testClass = new DummyTestWithNullClass();
        when(extensionContext.getRequiredTestClass()).thenAnswer(call -> DummyTestWithNullClass.class);
        when(extensionContext.getRequiredTestInstance()).thenReturn(testClass);

        //When TemporaryFolderExtension.beforeEach is called
        temporaryFolderExtension.beforeEach(extensionContext);

        //Then the temporaryFolder should be initialized
        assertNotNull(testClass.nullFolder);
        assertNotNull(testClass.nullFolder.getRoot());
    }

    @Test
    @DisplayName("Deletion after test")
    void it_should_delete_temporary_folder_after_test() throws Exception {
        //Given an extensionContext returning a test having as field a TemporaryFolder annotated with @TmpFolder
        DummyTestClass testClass = new DummyTestClass();
        when(extensionContext.getRequiredTestClass()).thenAnswer(call -> DummyTestClass.class);
        when(extensionContext.getRequiredTestInstance()).thenReturn(testClass);

        //When TemporaryFolderExtension.afterEach is called
        temporaryFolderExtension.afterEach(extensionContext);

        //Then the temporaryFolder should be deleted
        assertThrows(IllegalStateException.class, () -> testClass.folder.getRoot());
    }

}