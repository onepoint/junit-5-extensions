# Junit 5 extensions
[![pipeline status](https://gitlab.com/onepoint/junit-5-extensions/badges/master/pipeline.svg)](https://gitlab.com/onepoint/junit-5-extensions/commits/master)

This library provides several extensions for junit5. 

## Downloads

See usages on [mvnrepository.com](https://mvnrepository.com/artifact/fr.onepoint/junit-5-extensions/1.0)

## Mockito Extension

Warning, this extension is deprecated since there is an official implementation by Mockito, see  [mockito-junit-jupiter](https://mvnrepository.com/artifact/org.mockito/mockito-junit-jupiter/2.17.1)

To run this extension on a test class, use this annotation :

```java
@ExtendWith(MockitoExtension.class)
class MyTestClass{
    ...
}
```

This will invoke the method ```MockitoAnnotations.initMocks(myTestInstance)``` before each test case. Use cases and examples are available in [Mockito documentation](https://static.javadoc.io/org.mockito/mockito-core/2.2.28/org/mockito/MockitoAnnotations.html)

## TemporaryFolder Extension

Junit5 has removed the ```@Rule``` annotation, removing TemporaryFolder rule.
To Keep using this class, the TemporaryFolder Extension can be used with junit5.

The test will fail on startup if an annotated field is not assignable to TemporaryFolder.

To use the extension, use this annotation :

```java
@ExtendWith(TemporaryFolderExtension.class)
class MyTestClass{
    ...
}
```  

All the TemporaryFolder fields annotated with ```@TmpFolder``` will be initialized.

Exemple :

```java
@ExtendWith(TemporaryFolderExtension.class)
class BulletinsFermesServiceTest {

    @TmpFolder
    private TemporaryFolder workingFolder;
    
    @Test
    void some_test_case() {
        File f = workingFolder.newFile("myTemporaryFile.txt"); //create a new file in a temporaryFolder
        ...
    }
}
```

## Resource Extension

This extension can be used to get files in the resources folder easily.
The resource will be provided into Java Object instance. 
The supported classes list can be found [in the javadoc](https://onepoint.gitlab.io/junit-5-extensions/fr/onepoint/test/resource/ResourceExtension.html) 
The test will fail on startup if a resource can't be found or if the class type is not supported.

To use the extension, use this annotation :

```java
@ExtendWith(ResourceExtension.class)
class MyTestClass{
    ...
}
```  

Variables declared as fields or parameters and annotated with ```@ResourceFile``` are eligible to the extension.

```java
@ExtendWith(ResourceExtension.class)
class MyTestClass{
    
    @ResourceFile("someWebFile.html") 
    private File htmlFile;

    @BeforeEach
    void some_test_case(@ResourceFile("someFile.csv") File csvFile,
            @ResourceFile("someOtherFile.xml") File xmlFile,
            @ResourceFile("someFolder/someArchive.ZIP") Path adressZip){
        ...
    }

}
```  

## Javadoc

Javadoc can be found [here](https://onepoint.gitlab.io/junit-5-extensions) 

## License

This project is under MIT License. See [License file](LICENSE)